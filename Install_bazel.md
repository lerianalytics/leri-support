Install bazel without root
```
wget https://github.com/bazelbuild/bazel/releases/download/0.13.0/bazel-0.13.0-installer-linux-x86_64.sh
bash bazel-0.13.0-installer-linux-x86_64.sh --prefix=/home/user_name/local
```
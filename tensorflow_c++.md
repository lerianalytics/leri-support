https://matrices.io/training-a-deep-neural-network-using-only-tensorflow-c/

Download [bazel-x.xx.x.sh](https://github.com/bazelbuild/bazel/releases/download/0.17.2/bazel-0.17.2-installer-linux-x86_64.sh)
```
bazel-0.17.2-installer-linux-x86_64.sh  --prefix=/home/username/local/
bazel run -c opt <directory_of_codes>
```


Install SWIG  
sudo apt-get install swig  
cd tensorflow/tensorflow/contrib/cmake/  
mkdir build  
cd build  
cmake ..  
make -sj<number-of-threads> all  
make -sj<number-of-threads> install  
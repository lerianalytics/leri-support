

Install depedencies first.
```
yum install  libpng12-devel gd-devel  
wget gnuplot
./configure --prefix=/usr
make && make install
```

There is still something wrong in the font library, such as
Could not find/open font when opening font "arial", using internal non-scalable font

Possible solution:
```
wget http://www.itzgeek.com/msttcore-fonts-2.0-3.noarch.rpm
rpm -Uvh msttcore-fonts-2.0-3.noarch.rpm
```
Put followings into ~/.bashrc
```
export GDFONTPATH="/usr/share/fonts/msttcore:$GDFONTPATH"
export GNUPLOT_DEFAULT_GDFONT="arial"
```
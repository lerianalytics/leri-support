### Install
```
wget https://jaist.dl.sourceforge.net/project/pymol/pymol/1.7/pymol-v1.7.6.0.tar.bz2  
``` 
```
tar -xjf pymol-v1.7.6.0.tar.bz2   
```
```
cd pymol  
```
```
python setup.py build install 
```
Maybe require  
```
sudo apt-get install libglu1-mesa-dev freeglut3-dev mesa-common-dev
```
```
sudo apt-get install libglew-dev
```


```
cd /usr/lib/x86_64-linux-gnu
```
```
sudo ln -s -f libGL.so.1.0.0 libGL.so
```

### Alignment
```
 align structure2 and resi 1-100 and name n+ca+c+o, structure1 and resi 300-400 and name n+ca+c+o
```
Find more at http://pldserver1.biochem.queensu.ca/~rlc/work/pymol/


#### Create png of a PDB
```
protnam=$1  
target=$2  
PyMOL ${rootdir}/${protnam}.pdb ${rootdir}/${target}.pdb -cqd 'align '${protnam}', '${target}';   
hide all;   
show cartoon; 
align MODEL & i. 100-200, TARGET & i. 100-200, cutoff=1 
set cartoon_fancy_helices, 1 
set cartoon_fancy_sheets, 1 
color red, '${target}';   
select loops, resi 760-790;   
center loops;   
rotate x, 0;   
rotate y, 0;   
set depth_cue, 0;   
set opaque_background, off;   
ray 800, 800;   
png '${figdir}/${protnam}'-vs-Native.png;'  
#PyMOL ${rootdir}/lowRMSD-R4.pdb ${rootdir}/lowRMSD-R5.pdb -cqd 'align lowRMSD-R4, lowRMSD-R5; hide all; show cartoon; spectrum count, rainbow; select loops, resi 760-790; center loops; rotate x, 0; rotate y, 0; set depth_cue, 0; set opaque_background, off; ray 800, 800; png '${figdir}/${protnam}'.png;'  
#done  

for i in `cat list0`  
do  
protnam=$i  
PyMOL ${rootdir}/${protnam}.pdb -cd 'hide all; show cartoon; spectrum count, rainbow; select loops, resi 760-790; center loops; set depth_cue, 0; set opaque_background, off; ray 800, 800; png '${figdir}/${protnam}'.png;'  
done  
```

# Notes for installing Debian 9

During the installation of Debian 9, the following configurations are defined
Host: siva
Domain: leria.com

Partition (2T)  
/  -- 8G  
/boot -- 500M  
/opt  -- 5G  
/var  -- 150G  
/usr  -- 50G  
/usr/local -- 80G  
/tmp -- 20G  
/swap  -- 32G  
/home -- rest  



## Sudo
When the system is ready to launch, we may need to change or install some software, then it requires sudo. 
So we could change the access in sudoers for different users. 
```
su
apt install sudo -y
cd /etc
visudo # add user as sudoer
```
#### Install build-essentials
An essential package *build-essentials* should be installed to compile a Debian package.
It generally includes the GCC/g++ compilers and libraries and some other utilities. 
```
sudo apt install build-essential
```

#### Install GCC
It dependends on GMP, MPFR, MPC,and FLEX.
```
sudo apt install flex
#wget https://github.com/westes/flex/archive/v2.6.4.tar.gz
```
#### Install make
```
wget http://ftp.gnu.org/gnu/make/make-4.2.tar.gz
tar -xzf make-4.2.tar.gz
cd make-4.2
mkdir build && cd build
../configure
./build.sh
make install
```
#### Install cmake
```
wget https://cmake.org/files/v3.12/cmake-3.12.2.tar.gz
tar -xzf cmake-3.12.2.tar.gz
cd cmake-3.12.2
mkdir build && cd build
../configure
make -j16
make install

```

#### Install other packages
```
sudo apt-get install libprotobuf-dev libopencv-dev libhdf5-dev libeigen3-dev
sudo apt-get install --no-install-recommends libboost-all-dev
sudo apt-get install libgflags-dev libgoogle-glog-dev
sudo apt-get install libtbb-dev
```
#### Install Nvidia driver
Go to officical website to download it
Install Kernel file
```
sudo apt install -y -f linux-headers-$(uname -r)
```

#### Install Cuda-10
```
sudo apt-get install linux-source
sudo apt-get source linux-image-$(uname -r)
sudo apt-get install linux-headers-$(uname -r)
```
Run with --override to override compiler choice  
```
su
bash cuda_10.0.130_410.48_linux --override
```
Please make sure that  
 -   PATH includes /usr/local/cuda-10.0/bin  
 -   LD_LIBRARY_PATH includes /usr/local/cuda-10.0/lib64, or, add /usr/local/cuda-10.0/lib64 to /etc/ld.so.conf and run ldconfig as root  

To uninstall the CUDA Toolkit, run the uninstall script in */usr/local/cuda-10.0/bin*  
To uninstall the NVIDIA Driver, run *nvidia-uninstall*  

#### Install Apache2
```
sudo apt install apache2
```

#### Install Torque
```
sudo apt-get install libssl-dev
sudo apt-get install libssl1.0-dev
git clone https://github.com/adaptivecomputing/torque.git
./autogen.sh
```
vim configure, make all gccwarnings = no
```
./configure --enable-gcc-warnings \
            --enable-shared \
            --enable-static \
            --enable-fast-install \
            --enable-syslog \
            --enable-cgroups \
            --enable-unixsockets \
            --with-scp \
            --with-server-home=/var/spool/torque \
            --with-boost-path=/usr/share/doc/libboost-all-dev \
            CC="gcc -m64"

./configure --with-default-server=<your server name> --with-server-home=/var/spool/pbs --with-rcp=scp
make
sudo make install
```
Replace is_array with is_arrayX in 
src/include/array.h 
src/test/process_request/scaffolding.c
src/server/process_request.c
src/server/req_stat.c

#### Install Google Chrome
```
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
sudo apt-get update
sudo apt-get install google-chrome-stable
```
In incognito mode
```
sudo vi opt/google/chrome/google-chrome
```
(Maybe it can be found here: */usr/share/applications/\*google\**)  
Change *exec -a "$0" "$HERE/chrome" "$@"* to *exec -a "$0" "$HERE/chrome" "$@ --incognito"*

Alternatively, go to properties of Google Chrome, 
add **--incognito %U** in command option.

#### Install Sublime
```
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install apt-transport-https
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text
```

### Install net-tool
```
sudo apt install net-tools
sudo -ifconfig -a
```

Add a Public IP Address to a Linux Server
```
sudo vi /etc/network/interfaces
```

```
sudo apt install resolvconf
```
Add the lines as below:

```
# The primary network interface
auto eth0
iface eth0 inet static
    address 128.135.10.21
    netmask 255.255.255.128
    network 128.135.10.0
    broadcast 128.135.10.127
    gateway 128.135.10.1
#   # dns-* options are implemented by the resolvconf package, if installed
    dns-nameservers 128.135.247.50 128.135.249.50
    dns-search uchicago.edu
```


#### Parallel 
Should limit thread in the ~/.bashrc  
To get total number of threads:
```
ps -eL | wc -l
```
or 
```
awk '{split($4,a,"/");print a[2]}' /proc/loadavg
```



## Web-site & Web-server
When the system is ready to use, go to /etc and add 
<THE_IP_ADDRESS> \tab siva.leria.com siva

After install Apache2, website and server can be defined in /var/www/html/.
Type siva.leria.com/xxx, the website can be visited. 

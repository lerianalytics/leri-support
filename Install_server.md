Install Apache
```
yum clean all  
yum -y update  
yum -y install httpd  
```
Allow Apache Through the Firewall
```
firewall-cmd --permanent --add-port=80/tcp  
firewall-cmd --permanent --add-port=443/tcp  
```

Configure Apache to Start on Boot
```
systemctl start httpd  
systemctl enable httpd  
```

Stop firewall
```
systemctl stop firewalld
```

Install Torwque 
```
yum install  libtool openssl-devel libxml2-devel  
autogen.sh  
configure  
make -j16 && make install

```
Confgiuration:
```
vi /etc/sysconfig/network
# add HOSTNAME=sibe
/etc/rc.d/init.d/network restart

vi /etc/hosts
# add IP sibe

echo "sibe" > /var/spool/torque/server_name

Populate /var/spool/torque/server_priv/nodes. A basic example follows:
node01 np=1
node02 np=1
```





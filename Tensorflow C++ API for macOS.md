# Tensorflow C++ API for macOS

## Download and build Tensorflow
Download the tensorflow repo from Github and run configure:
```
git clone https://github.com/tensorflow/tensorflow.git
cd tensorflow
mkdir build && cd build
../configure
```
After configuration, build the **tensorflow_cc.so** library.
There are serval different optimization that can be chosen. 
Generally, the following command works on many machines.
```
bazel build --config=monolithic //tensorflow:libtensorflow_cc.so
```
Particularly, you can pick which CPU extensions that are supported, e.g.:
```
bazel build -c opt --copt=-mavx --copt=-mavx2 --copt=-mfma  --copt=-msse4.2 \
--config=monolithic tensorflow:libtensorflow_cc.so
```
**Warn**: the default choice (�config=opt) is the worst choice if you want 
to ship the final program and be compatible with other processors.

For the first time of compilation, it will take much long time to build. 
Please be patient to wait. 

## Libraries
When it is completed, it is time to install the header and library files in 
<user-defined> location, e.g. **/usr/local/**. In the tensorflow source folder,
run the commands:
```
cd bazel-bin/tensorflow
install_name_tool -ld "@loader_path/libtensorflow_cc.so" libtensorflow_cc.so
```
Then install the libirary as an example as follows,
```
cp bazel-bin/tensorflow/libtensorflow_cc.so /usr/local/lib/libtensorflow_cc.so
```

## Headers
It will save space if we just copy necessary header files.
```
dirt=/usr/local/include/tensorflow/include
cp -RL ./tensorflow/core $dirt/tensorflow
cp -RL ./tensorflow/cc $dirt/tensorflow

mkdir -p $dirt/third_party
cp -RL ./third_party/eigen3 $dirt/third_party

#rm -rf $dirt/third_party/eigen3/unsupported
cp -RLf ./bazel-tensorflow/external/eigen_archive/unsupported $dirt

cp -RL ./bazel-genfiles/tensorflow/cc $dirt/tensorflow
cp -RL ./bazel-genfiles/tensorflow/core $dirt/tensorflow

cp -RL ./bazel-tensorflow/external/eigen_archive/Eigen $dirt/Eigen
```

## Others
Get the source from nsync, Eigen and google protobuffer by running:
```
tensorflow/contrib/makefile/download_dependencies.s
```
The downloaded files can be found in
```
tensorflow/contrib/makefile/downloads/
```

## Test C++ code
main.cpp
```
#include <iostream>
#include <vector>
#include "tensorflow/cc/client/client_session.h"
#include "tensorflow/cc/ops/standard_ops.h"

int main() {

    using namespace tensorflow;
    using namespace tensorflow::ops;
    Scope root = Scope::NewRootScope();

    auto A = Const(root, {{1.f, 2.f}, {3.f, 4.f}});
    auto b = Const(root, {{5.f, 6.f}});
    auto x = MatMul(root.WithOpName("v"), A, b, MatMul::TransposeB(true));
    std::vector<Tensor> outputs;

    std::unique_ptr<ClientSession> session = std::make_unique<ClientSession>(root);
    TF_CHECK_OK(session->Run({x}, &outputs));
    std::cout << outputs[0].matrix<float>();

}
```

CMakefile.txt
```
cmake_minimum_required(VERSION 3.9)
project(tftest)

set(CMAKE_CXX_STANDARD 14)

include_directories(
        /usr/local/include/tensorflow/include
        /usr/local/include/google/include
        /usr/local/include/tensorflow/include/external/nsync/public)

link_directories(/usr/local/include/tensorflow/lib/macOS)
add_executable(tftest main.cpp )
target_link_libraries(tftest tensorflow_cc)
```

Output should be:
```
tftest
17
39
```
From https://serverfault.com/questions/319051/reaching-a-linux-server-with-a-private-ip-without-vpn

Assuming that box A is your desktop, box P is the server with the private IP, and box C is the cloud server:

on P, open the tunnel:

P% ssh C -R 2200:localhost:22
Leave that session running.

To connect later from A:

A% ssh C
C% ssh localhost -p 2200
P%    # you're now logged into P
This only works as long as noone interrupts that first ssh session from P to C, but as long as they don't, it does work.
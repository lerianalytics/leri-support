




on Godzilla

Submit a job when use Leri

Define a test.sh as follows,

```
#!/bin/bash

LD_LIBRARY_PATH=/home/user_name/local/gcc-6.3.0/lib:/home/user_name/local/gcc-6.3.0/lib64:$LD_LIBRARY_PATH \
/home/user_name/leri-analytics/leri-build/tool/leri sequence_coupling -msa /home/user_name/leri-analytics/leri/example/msa/ww_msa_trimmed.aln -threads 1 -jobname test_coupling
```
Then 

qsub -cwd -q ibi.q -q intel12cores.q -o <name_of_log>.log test.sh



After installing GCC, go to ~/.bashrc

# GCC
export PATH=/home2/ngaam/local/gmp-6.1.2/bin:$PATH
export LD_LIBRARY_PATH=/home2/ngaam/local/gmp-6.1.2/lib:$LD_LIBRARY_PATH
export PATH=/home2/ngaam/local/mpfr-3.1.5/bin:$PATH
export LD_LIBRARY_PATH=/home2/ngaam/local/mpfr-3.1.5/lib:$LD_LIBRARY_PATH
export PATH=/home2/ngaam/local/mpc-1.0.3/bin:$PATH
export LD_LIBRARY_PATH=/home2/ngaam/local/mpc-1.0.3/lib:$LD_LIBRARY_PATH
export PATH=/home2/ngaam/local/gcc-6.3.0/bin:$PATH
export LD_LIBRARY_PATH=/home2/ngaam/local/gcc-6.3.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home2/ngaam/local/gcc-6.3.0/lib64:$LD_LIBRARY_PATH
export CC=/home2/ngaam/local/gcc-6.3.0/bin/gcc
export CXX=/home2/ngaam/local/gcc-6.3.0/bin/g++
# Make
#export PATH=/home2/ngaam/local/make-4.2/bin:$PATH
#export LD_LIBRARY_PATH=/home2/ngaam/local/make-4.2/lib:$LD_LIBRARY_PATH
#export make=/home2/ngaam/local/make-4.2/bin/make
#export PATH=/home2/ngaam/local/cmake-3.10.0-rc1/bin:$PATH
#export LD_LIBRARY_PATH=/home2/ngaam/local/cmake-3.10.0-rc1/lib:$LD_LIBRARY_PATH
#export cmake=/home2/ngaam/local/cmake-3.10.0-rc1/bin/make



LOCAL_DIRT=/home2/ngaam/local

# Install Gflags

mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=${LOCAL_DIRT} ..
vi CMakeCache.txt # edit CMAKE_CXX_FLAGS:STRING=-fPIC
make 
make install


# Install Glog
./configure --prefix=${LOCAL_DIRT}

make 
make install

# OpenCV
mkdir build
cd build 
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=${LOCAL_DIRT}  -D BUILD_NEW_PYTHON_SUPPORT=ON ..

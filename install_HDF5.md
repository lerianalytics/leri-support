Under ```sudo```
```
./configure --prefix=/usr/local/hdf5 
make -j16 
sudo make install
```

Better to copy the installed files to those folders under /usr/local
```
sudo cp /usr/local/hdf5/lib/* /usr/local/lib/ 
sudo cp /usr/local/hdf5/include/* /usr/local/include/ 
sudo cp /usr/local/hdf5/bin/* /usr/local/bin/ 
sudo cp -r /usr/local/hdf5/share/* /usr/local/share/ 
```

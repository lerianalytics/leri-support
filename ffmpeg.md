`./configure --enable-gpl --enable-postproc --enable-swscale --enable-pthreads --enable-libdc1394 --enable-libx264 --enable-nonfree`  
`make -j4`  
`make install`  
`ffmpeg -i source.wmv/avi -vcodec libx264 -crf 16 output.mp4` 

#Join video
`ffmpeg -f concat -i lst -c copy he0831.wmv`  
`cat lst`  
file '1.mp4/wmv'  
file '2.mp4/wmv'  
file '3.mp4/wmv'  

`ffmpeg -i input.mp4 -vcodec libx265 -crf 28 output.mp4`
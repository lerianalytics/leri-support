### Boost
```
cmake \
-DBoost_NO_SYSTEM_PATHS=TRUE \
-DBoost_NO_BOOST_CMAKE=TRUE \
-DBOOST_ROOT=/home/usrname/local/include \
-DBoost_LIBRARY_DIRS:FILEPATH=/home/username/local/lib \
../leri
```

### HDF5
```
configure --prefix=/home/usrname/local/hdf5
make
make install
```
If '--prefix' doesn't work, copy files in the directory 'hdf5' to /usr/local or other costumized place.


### leveldb
```
git clone https://github.com/google/leveldb.git                  
cd leveldb/ 
mkdir build
cmake -DCMAKE_INSTALL_PREFIX:PATH=/home/username/local ..
make -j16 && make install
```

### LMDB
```
git clone https://gitorious.org/mdb/mdb.git
cd mdb/libraries/liblmdb
vi Makefile # change prefix to /home/you/usr
make ; make install
```

### Snappy
```
git clone https://github.com/google/snappy.git                                                                                                                
cd snappy/   
vi option(SNAPPY_BUILD_TESTS "Build Snappy's own tests." OFF)
mkdir build
cmake -DCMAKE_INSTALL_PREFIX:PATH=/home/username/local ..                   
make -j16 && make install
```

### Lapack & Atlas
Download [lapack-3.4.2.tgz](http://www.netlib.org/lapack/lapack-3.4.2.tgz) and [atlas3.10.0.tar.bz2](http://downloads.sourceforge.net/project/math-atlas/Stable/3.10.0/atlas3.10.0.tar.bz2?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fmath-atlas%2Ffiles%2FStable%2F3.10.0%2F&ts=1425823749&use_mirror=hivelocity)
```
mkdir build
cd build
../configure -b 64 -Fa alg -fPIC --shared --prefix=/home/usernam/local/ --with-netlib-lapack-tarfile=/directory/to/lapack-3.4.2.tgz 
make -j16 && make install
```

### OpenCV
```
curl -L -O http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.10/opencv-2.4.10.zip 
unzip opencv-2.4.10.zip                                          
cd opencv-2.4.10
mkdir build                                                      
cd build                                                         
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/home/usernam/local ..
make -j16 && make install
```



## Maybe need to install 

### Autoconf
```
curl -L -O http://mirror.koddos.net/gnu/autoconf/autoconf-2.10.tar.gz
configure --prefix=/home/usernam/local 
make install
```
### Automake
```
configure --prefix=/home/usernam/local 
make install
```
